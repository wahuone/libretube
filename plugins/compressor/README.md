## Descripción

Es un complemento para Pelican que minifica los archivos CSS y
JavaScript. Reemplaza los archivos JS y CSS por los archivos
minificados al momento de la compilación del sitio web.

## Instalación

Añade `compressor` a `pelicanconf.py`: `PLUGINS =
['compressor']`.
