pelican_js_css
===============

This Pelican plugin makes it easy to embed Javascript files and CSS stylesheets into individual Pelican blog articles.

Credit
------
This plugin was inspired by and adopted from the work of [Rob Story](https://github.com/wrobstory) at [pelican_dynamic](https://github.com/wrobstory/pelican_dynamic). I decided to create a similar project on my own instead of forking that one because I'd like to make breaking changes.

Installation
------------
To install the plugin, [follow the instructions on the Pelican plugin page.](https://github.com/getpelican/pelican-plugins) My settings look like the following:

```python
PLUGIN_PATH = 'pelican-plugins'
PLUGINS = ['pelican_css_js']
```

Directory Structure
-------------------
Create ```js``` and ```css``` directories in your ```content``` directory:
```
website/
├── content
│   ├── js/
│   │   └── demo1.js
│   │   └── demo2.js
│   ├── css/
│   │   └── demo1.css
│   ├── article1.rst
│   ├── cat/
│   │   └── article2.md
│   └── pages
│       └── about.md
└── pelicanconf.py
```

and then specify each resource as a comma-separated file name in the ```JavaScripts``` and ```StyleSheets``` keys: (note that these key names are case-insensitive, so ```javascripts``` and ```stylesheets``` would work just fine)

```
Title: Chuletas de programación
Date: 2019-01-20
Lang: es
Slug: chuletas-de-programacion
Author: Jesús E.
JS: demo.js, demo2.js
Styles: demo.css, demo2.css
```

You can also include javascript libraries from a web resource without having to carry the files on your own. If the string starts with `http://` or `https://` it will be treated like a web resource, otherwise the plugin will look for the file under `content/js` for javascript files, and `content/css` for CSS stylesheets.

```
Title: Chuletas de programación
Date: 2019-01-20
Lang: es
Slug: chuletas-de-programacion
Author: Jesús E.
JS: https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js, demo.js, demo2.js
Styles: demo.css, demo2.css
```

Note that the files will be included in the same order specified.

Additions to Templates
----------------------
Finally, in your base template (likely named ```base.html```), you need to add the following in your ```<head>``` section of the HTML:
```
{% if article %}
    {% if article.styles %}
        {% for style in article.styles %}
{{ style }}
        {% endfor %}
    {% endif %}
{% endif %}
```
and the following *after* your ```</body>``` tag:
```
{% if article %}
    {% if article.js %}
        {% for script in article.js %}
{{ script }}
        {% endfor %}
    {% endif %}
{% endif %}
```

That's it! Run your standard ```make html``` or ```make publish``` commands and your JSS/CSS will be moved and referenced in the output HTML.
