Tipue Search
============

A Pelican plugin to serialize generated HTML to JSON.

- Copyright (c) Talha Mansoor
- Copyright (c) 2020 - Jesús E.

Original Authority
=================

Author          | Talha Mansoor
----------------|-----
Author Email    | talha131@gmail.com
Author Homepage | http://onCrashReboot.com
Github Account  | https://github.com/talha131

Modification
============

Author          | Jesús E.
----------------|-----
Author Email    | heckyel@hyperbola.info
Author Homepage | https://conocimientoslibres.tuxfamily.org/
Github Account  | https://libregit.org/heckyel

Why do you need it?
===================

Static sites do not offer search feature out of the box. [Tipue Search](http://www.tipue.com/search/)
is a jQuery plugin that search the static site without using any third party service, like DuckDuckGo or Google.

Tipue Search offers 4 search modes. Its [JSON search mode](http://www.tipue.com/search/docs/json/) is the best search mode
especially for large sites.

Tipue's JSON search mode requires the textual content of site in JSON format.

Requirements
============

Tipue Search requires BeautifulSoup.

```bash
pip install beautifulsoup4
```

How Tipue Search works
=========================

Tipue Search serializes the generated HTML into JSON. Format of JSON is as follows

```python

  "videos": [
    {
      "title": "Usar cuenta Jabber/XMPP",
      "description": "Usar cuenta Jabber/XMPP Usar una cuenta de Jabber/XMPP es muy fácil. Instalar gajim en Hyperbola GNU/Linux-libre: $ sudo pacman -S gajim python2-axolotl Mostrar Más ¿Qué es Jabber/XMPP? Extensible Messaging and Presence Protocol, más conocido como XMPP (Protocolo extensible de mensajería y comunicación de presencia) (anteriormente llamado Jabber​), es un protocolo abierto y extensible basado en XML, originalmente ideado para mensajería instantánea. Con el protocolo XMPP queda establecida una plataforma para el intercambio de datos XML que puede ser usada en aplicaciones de mensajería instantánea. Las características en cuanto a adaptabilidad y sencillez del XML son heredadas de este modo por el protocolo XMPP. A diferencia de los protocolos privativos de intercambio de mensajes como ICQ, Y! y Windows Live Messenger, se encuentra documentado y se insta a utilizarlo en cualquier proyecto. Existen servidores y clientes libres que pueden ser usados sin coste alguno. Tras varios años de su existencia, ha sido adoptado por empresas como Facebook, WhatsApp Messenger y Nimbuzz, entre otras, para su servicio de chat.",
      "videoThumbnail": "https://lablibre.tuxfamily.org/theme/../wp-content/uploads/article/poster/2020/05/usar-xmpp.jpeg",
      "formatStreams": {
        "url": "https://archive.org/download/libreweb/uso-de-cuenta-xmpp.webm"
      },
      "author": "Jesús E.",
      "authorUrl": "https://lablibre.tuxfamily.org/author/jesus-e/",
      "published": "2020-05-20T15:23:44+02:00",
      "publishedText": "mié, 20 mayo, 2020",
      "time": "16:51",
      "tags": "Tutorial",
      "url": "https://lablibre.tuxfamily.org/usar-cuenta-jabber-xmpp/"
    },
    {
      "title": "Happy Birthday To GNU",
      "description": "Happy Birthday To GNU Sr. Stephen Fry introduces you to free software, and reminds you of a very special birthday. In this film, Stephen Fry, the actor, humorist and author introduces the viewer to free software and GNU. Along the way, he compares proprietary software to the 'tyranny' of scientific research that cannot be shared, studied and distributed by others. Show More Credits of video Free distros",
      "videoThumbnail": "https://lablibre.tuxfamily.org/theme/../wp-content/uploads/article/poster/2008/09/happy-gnu.jpg",
      "formatStreams": {
        "url": "https://archive.org/download/libreweb/sf-large.webm"
      },
      "author": "Free Software Foundation",
      "authorUrl": "https://lablibre.tuxfamily.org/author/free-software-foundation/",
      "published": "2008-09-02T21:04:48+02:00",
      "publishedText": "mar, 02 septiembre, 2008",
      "time": "5:51",
      "tags": "GNU",
      "url": "https://lablibre.tuxfamily.org/happy-birthday-to-gnu/"
    }
  ]
}
```

JSON is written to file `tipuesearch_content.json` which is created in the root of `output` directory.

How to use
==========

To utilize JSON Search mode, your theme needs to have Tipue Search properly configured in it. [Official documentation](http://www.tipue.com/search/docs/#json) has the required details.

Pelican [Elegant Theme](https://github.com/talha131/pelican-elegant) and [Plumage
theme](https://github.com/kdeldycke/plumage) have Tipue Search configured. You can view their
code to understand the configuration.
