App version
============

A Pelican plugin to show app version from git.

Copyright (c) 2020 - Jesús E.

How to use
==========

In your pelicanconf.py, add:

    PLUGINS = ['get_app_version']

In your template, add:

    {% if CURRENT_VERSION and CURRENT_BRANCH %}
      <h6>{{ _('Current version:') }} {{ CURRENT_VERSION }} @ {{ CURRENT_BRANCH }}</h6>
    {% endif %}

License
=======

This work is under the License [GNU GPLv3+](LICENSE)
