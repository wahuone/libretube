Author: Jesús E.
Date: 2019-02-04 07:36
Lang: es
Modified: 2019-02-04 07:38
Slug: contacto
Status: published
Title: Contacto

Mi correo electrónico es
[heckyel@hyperbola.info](mailto:heckyel@hyperbola.info). Utiliza mi
clave pública <abbr title="GNU Privacy Guard">GPG</abbr>
([4DF2 1B6A 7C10 21B2 5C36 0914 F6EE 7BC5 9A31 5766]({filename}/heckyel_pub.asc))
para evitar que el mensaje pueda ser leído por otras personas.
