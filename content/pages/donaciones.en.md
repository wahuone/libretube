Author: Jorge Maldonado Ventura
Date: 2018-03-29 00:29
Lang: en
Slug: donaciones
Status: hidden
Title: Donations

<table>
    <tr>
        <th>Date</th>
        <th>Donor</th>
        <th>Currency</th>
        <th>Amount</th>
    </tr>
    <tr>
        <td>2018-02-12</td>
        <td>Anonymous</td>
        <td>Faircoin</td>
        <td>0.008</td>
    </tr>
</table>
