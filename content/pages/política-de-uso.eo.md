Author: Jesús E.
Date: 2018-08-08 18:14
Lang: eo
Save_as: pages/regularo-pri-uzado
Slug: política-de-uso
Status: hidden
Title: Regularo pri uzado
Url: pages/regularo-pri-uzado

Oni akceptas sugestojn kaj aliigojn de ĉi tiu regularo adresitaj per la
[koncernaĵo-administrilo de LibreTube](https://libregit.org/heckyel/libretube/issues)
aŭ per [nia kontakta retpoŝto]({filename}/pages/contacto.md).

## Kondiĉaro pro uzado

Ni ne akceptas la respondecon pri la eblaj problemoj derivitaj de la
uzado de nia retpaĝaro. Ni akceptas kritikojn kaj korrektojn por pliboni
la paĝaron kaj solvi la eblajn erarojn, kiujn ni havigu.

LibreTube respektas la opiniojn, kritikojn kaj proponojn esprimitaj en
komentoj. Ni havas la rajton forigi la spamojn.

Kiam ebla, ni penas fari la paĝaron alirebla al pli granda nombro de
homoj eble: al malkapablaj homoj; al kiu retumas kun Ĝavaskripto
maleblita; al homoj, kiu uzas teksto-retumilojn; kun multa trafiklimigo,
ktp.

Kie ne notita kontraŭon, la licencoj de LibreTube estas la
[CC0](https://creativecommons.org/publicdomain/mark/1.0/deed.eo) de
publikaĵo, por la enhavo (tekstoj, bildoj, videoj, komentoj...), kaj la
<abbr title="Affero General Public License, version 3">AGPLv3</abbr> de
libera programaro, por la programaro. LibreTube Lankaŭ uzas
programaron farita de aliaj, kiu povas trovi je alia licenco de libera
programaro, konsultu [informon pri
licencoj](https://libregit.org/heckyel/libretube#informaci%C3%B3n-de-licencias)
por pli informo pri la programaraj licencoj.

## Regularo pri privateco

LT estas gastita en [Tuxfamily](https://tuxfamily.org/).
Ci tiu provizanto de gastigo uzas
[Nginx](https://eo.wikipedia.org/wiki/Nginx)-servilon, kiu
gastigas en dosieroj aliro-datumoj: <abbr title="Interreta
Protokolo">IP</abbr>, uzita retumilo, dato de vizito, ktp. Ĉi tiu
informo estas malpersona, sed oni povus ritali al vizitantaj homoj. Oni
povas viziti la retpaĝaron kun prokurilo kiel
[Tor](https://eo.wikipedia.org/wiki/Tor_(programaro)) aŭ
<a href="https://eo.wikipedia.org/wiki/VPN"><abbr title="Virtuala Privata Reto">VPN</abbr></a> por havi pli privatecon.

Pri la artikoloj oni povas fari anonimajn komentojn, kun falsaj nomoj aŭ
kun realaj datumoj. Okaze de ajno volu forigi aŭ rektifi komenton, ri devu
[kontakti nin]({filename}/pages/contacto.md)
demonstranta, kiu vi skribis la komenton, en tiu kazo ni indikos, ke oni
eliminis aŭ modifis la komenton kiel ni trovis tion bone, ĉiam penante
esti plej travidebla eble kun la aliaj partoprenantoj de la konversacio
kaj evitante perdi la ĉirkaŭtekston de aliaj komentoj.
