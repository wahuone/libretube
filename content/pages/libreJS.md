Author: Jesús E.
Date: 2019-04-01 19:46
Modified: 2019-04-28 10:03
Slug: librejs
Status: hidden
Title: LibreJS

<table id="jslicense-labels1" class="table table-striped table-dark text-center">
    <caption class="text-center mb-2">Tabla de Licencias de JavaScript</caption>
    <thead>
        <tr>
            <th>Archivo</th>
            <th>Licencia</th>
            <th>Fuente</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><a href="../../theme/modules/plyr/plyr.min.js">plyr.min.js</a></td>
            <td><a href="https://www.freebsd.org/copyright/freebsd-license.html">Expat</a></td>
            <td><a href="https://libregit.org/heckyel/plyr/archive/v2.0.3.tar.gz">plyr.tar.gz</a></td>
        </tr>
        <tr>
            <td><a href="../../theme/js/play.js">play.js</a></td>
            <td><a href="http://www.gnu.org/licenses/gpl-3.0.html">GPL-3.0</a></td>
            <td><a href="../../theme/js/play.js">play.js</a></td>
        </tr>
        <tr>
            <td><a href="../../hashover-next/comments.php">hashover.js</a></td>
            <td><a href="http://www.gnu.org/licenses/agpl-3.0.html">AGPL-3.0</a></td>
            <td><a href="../../hashover-next/comments.php">hashover.js</a></td>
        </tr>
    </tbody>
</table>
