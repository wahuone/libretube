Author: Jesús E.
Date: 2019-02-04 07:36
Lang: eo
Slug: contacto
Status: published
Title: Kontakto

Mia retpoŝto estas
[heckyel@hyperbola.info](mailto:heckyel@hyperbola.info). Uzu mian
<abbr title="GNU Privacy Guard">GPG</abbr> publikan ŝlosilon
([4DF2 1B6A 7C10 21B2 5C36 0914 F6EE 7BC5 9A31 5766]({filename}/heckyel_pub.asc))
por ke aliaj personoj ne povus legi la mesaĝon.
