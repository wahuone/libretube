Author: Jesús E.
Date: 2016-07-28 17:43
Lang: eo
Modified: 2017-04-27 16:15
Slug: acerca-de
Status: hidden
Title: Pri

## Kio estas LibreTube?

LibreTube estas videa eldona platformo sen kodiga procezo
sur la servilo. I estas simpla kaj simpla retejo por la prezentado de videoj.

## Kunlaborado

Vi povas skribi artikulojn por ĉi tia retpaĝaro, traduki aŭ kunlabori
kun la programado. En la
[legumino da fonta kodo da retpaĝaro](https://libregit.org/heckyel/libretube/src/master/README.markdown#colaboraci%C3%B3n)
estas la informo pri kiel fari ĝin.
