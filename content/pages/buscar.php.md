Author: Jorge Maldonado Ventura
Date: 2017-04-22 20:38
Modified: 2020-06-11 11:38
Save_as: buscar.php
Status: hidden
Title: Resultados

<main class="row main-videos">
<?php
$STOP_WORDS = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
               'ñ', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
               'un', 'una', 'unas', 'unos', 'uno', 'sobre', 'todo',
               'también', 'tras', 'otro', 'algun', 'alguno', 'alguna', 'algunos',
               'algunas', 'ser', 'es', 'soy', 'eres', 'somos', 'sois', 'estoy', 'esta',
               'estamos', 'estais', 'estan', 'como', 'en', 'para', 'atras', 'porque',
               'por', 'que', 'estado', 'estaba', 'ante', 'antes', 'siendo', 'ambos',
               'pero', 'por', 'poder', 'puede', 'puedo', 'podemos', 'podeis', 'pueden',
               'fui', 'fue', 'fuimos', 'fueron', 'hacer', 'hago', 'hace', 'hacemos',
               'haceis', 'hacen', 'cada', 'fin', 'incluso', 'primero', 'desde',
               'conseguir', 'consigo', 'consigue', 'consigues', 'conseguimos',
               'consiguen', 'ir', 'voy', 'va', 'vamos', 'vais', 'van', 'vaya', 'gueno',
               'ha', 'tener', 'tengo', 'tiene', 'tenemos', 'teneis', 'tienen', 'el', 'la',
               'lo', 'las', 'los', 'su', 'aqui', 'mio', 'tuyo', 'ellos', 'ellas', 'nos',
               'nosotros', 'vosotros', 'vosotras', 'si', 'dentro', 'solo', 'solamente',
               'saber', 'sabes', 'sabe', 'sabemos', 'sabeis', 'saben', 'ultimo', 'largo',
               'bastante', 'haces', 'muchos', 'aquellos', 'aquellas', 'sus', 'entonces',
               'tiempo', 'verdad', 'verdadero', 'verdadera', 'cierto', 'ciertos',
               'cierta', 'ciertas', 'intentar', 'intento', 'intenta', 'intentas',
               'intentamos', 'intentais', 'intentan', 'dos', 'bajo', 'arriba', 'encima',
               'usar', 'uso', 'usas', 'usa', 'usamos', 'usais', 'usan', 'emplear',
               'empleo', 'empleas', 'emplean', 'ampleamos', 'empleais', 'valor', 'muy',
               'era', 'eras', 'eramos', 'eran', 'modo', 'bien', 'cual', 'cuando', 'donde',
               'mientras', 'quien', 'con', 'entre', 'sin', 'trabajo', 'trabajar',
               'trabajas', 'trabaja', 'trabajamos', 'trabajais', 'trabajan', 'podria',
               'podrias', 'podriamos', 'podrian', 'podriais', 'yo', 'aquel'];

$web_content = json_decode(file_get_contents('tipuesearch_content.json'), true);
$stop_words_ignored = false;

if (isset($_GET['q']) && !empty($_GET['q'])) {

    $search_str = trim($_REQUEST['q']);

    $keywords = explode(' ', $search_str);
    $keywords_temp = NULL;
    foreach ($keywords as $keyword) {
        $is_stop_word = false;
        foreach ($STOP_WORDS as $stop_word) {
            if ($keyword == $stop_word) {
                $is_stop_word = true;
                $stop_words_ignored = true;
                break;
            }
        }
        if (! $is_stop_word) {
            $keywords_temp .= "{$keyword} ";
        }
    }

    $keywords = trim($keywords_temp);
    $keywords = explode(' ', $keywords);
    $found_results = [];

    foreach ($web_content["videos"] as $page) {
        $score = 0;
        $page['description'] = htmlentities($page['description']);

        // convert keyworks_tags to strings
        $tags = implode(", ", $page['keywords']);

        foreach ($keywords as $word) {
            if (preg_match("/$word/i", $page['url'])) {
                $score += 35;
            }
            if (preg_match("/$word/i", $page['title'])) {
                $score += 35;
            }
            if (preg_match("/$word/i", $tags)) {
                $score += 35;
            }
            if (preg_match("/$word/i", $page['category'])) {
                $score += 30;
            }
            // It replaces uppercase matches with lowercase matches, but it's fine for now.
            if ($stop_words_ignored == 1) {
                $page['description'] = preg_replace("/$word/i", $word, $page['description'], -1, $match_count);
            } else {
                $page['description'] = preg_replace("/$word/i", $word, $page['description'], -1, $match_count);
            }
            if ($match_count > 0) {
                $score += 10 * $match_count;
            }

        }
        if ($score != 0) {
            $found_results[] = [
                'score' => $score,
                'title' => $page['title'],
                'time' => $page['time'],
                'videoThumbnail' => $page['videoThumbnail'],
                'url' => $page['url'],
                'published' => $page['published'],
                'publishedText' => $page['publishedText'],
                'author' => $page['author'],
                'authorUrl' => $page['authorUrl'],
            ];
        }
    }

    /**
     * Compare results score.
     */
    function comp_result_score($a, $b) {
        if ($a['score'] == $b['score']) {
            return 0;
        }
        return ($a['score'] > $b['score']) ? -1 : 1;
    };

    if ($stop_words_ignored) {
        printf('
        <div class="col-md-12 text-justify">
            <h6 class="tab">%s</h6>
            <hr>
        </div>', 'Algunas palabras «comunes» fueron ignoradas');
    }

    $found_results_count = count($found_results);

    if ($found_results_count > 0) {
        usort($found_results, 'comp_result_score');
        if ($found_results_count == 1) {
            $found_results_count_str = NULL;
        }
    } else if ($found_results_count == 0) {
        $found_results_count_str = NULL;
        printf('<div id="tipue_search_warning">%s</div>', 'No se ha encontrado nada');
    }

    // Init head
    printf('
    <div class="col-md-12">
        <div class="row">');
        foreach ($found_results as $found_result) {
            printf('
            <article class="col-md-3 video" itemscope itemtype="https://schema.org/Movie">
                <a href="%s">
                    <div class="area">
                        <div class="mask">
                            <div class="vertical-align">
                                <i class="soumaicon play"><svg><use href="/theme/images/svg/master.svg#play"></use></svg></i>
                            </div>
                        </div>
                        <img itemprop="image" src="%s" alt="%s" class="img-fluid">
                        <span class="duration">%s</span>
                    </div>
                </a>
                <h2 itemprop="name">
                    <a href="%s" rel="bookmark" title="Ver %s">%s</a>
                </h2>
                <!-- post footer -->
                <footer class="card-content-footer mb-4">
                    <span class="soumaicon text-info">
                        <svg>
                            <use href="/theme/images/svg/master.svg#calendar"></use>
                        </svg>
                    </span>
                    <time class="entry-date published" datetime="%s" itemprop="datePublished">
                        <small>%s</small>
                    </time>
                    <span class="soumaicon text-info">
                        <svg>
                            <use href="/theme/images/svg/master.svg#user"></use>
                        </svg>
                    </span>
                    <small itemprop="director" itemscope itemtype="https://schema.org/Person">
                        <a href="%s" title="Autor del artículo" itemprop="name">%s</a>
                    </small>
                </footer>
                <!-- end of post footer -->
            </article>',
                   $found_result['url'], $found_result['videoThumbnail'], $found_result['title'],
                   $found_result['time'], $found_result['url'], $found_result['title'],
                   $found_result['title'], $found_result['published'], $found_result['publishedText'],
                   $found_result['authorUrl'], $found_result['author']);
        }

} else {
    printf('<div id="tipue_search_warning">%s</div>', 'Aún no has buscado');
}
// End head
printf('</div>
    </div>');
?>
</main>
