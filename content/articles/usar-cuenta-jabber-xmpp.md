Author: Jesús E.
Category: Tutorial
Date: 2020-05-20 15:23:44
Image: 2020/05/usar-xmpp.jpeg
Og_video: https://archive.org/download/libreweb/uso-de-cuenta-xmpp.webm
Slug: usar-cuenta-jabber-xmpp
Tags: xmpp, jabber, conversations, pix-art, gajim
Time: 16:51
Title: Usar cuenta Jabber/XMPP

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://ia803102.us.archive.org/7/items/libreweb/uso-de-cuenta-xmpp.webm" type="video/webm">
  </video>
  <figcaption>
    <h1>Usar cuenta Jabber/XMPP</h1>
  </figcaption>
</figure>

Usar una cuenta de Jabber/XMPP es muy fácil.

Instalar `gajim` en Hyperbola GNU/Linux-libre:

    $ sudo pacman -S gajim python2-axolotl

<details markdown="span">
<summary>Mostrar Más</summary>
<h6>¿Qué es Jabber/XMPP?</h6>
<p class="mt-2">Extensible Messaging and Presence Protocol, más conocido como XMPP
(Protocolo extensible de mensajería y comunicación de presencia)
(anteriormente llamado Jabber​), es un protocolo abierto y extensible
basado en XML, originalmente ideado para mensajería instantánea.</p>
<p class="mt-2">Con el protocolo XMPP queda establecida una plataforma para el
intercambio de datos XML que puede ser usada en aplicaciones de
mensajería instantánea. Las características en cuanto a adaptabilidad
y sencillez del XML son heredadas de este modo por el protocolo XMPP.</p>
<p class="mt-2">A diferencia de los protocolos privativos de intercambio de mensajes
como ICQ, Y! y Windows Live Messenger, se encuentra documentado y se
insta a utilizarlo en cualquier proyecto. Existen servidores y
clientes libres que pueden ser usados sin coste alguno.</p>
<p class="mt-2">Tras varios años de su existencia, ha sido adoptado por empresas como
Facebook, WhatsApp Messenger y Nimbuzz, entre otras, para su servicio
de chat.</p>
</details>
