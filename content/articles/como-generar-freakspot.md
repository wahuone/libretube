Author: Jesús E.
Category: Desarrollo web
Date: 2018-02-15 08:08
Image: 2017/12/como-generar-freakspot.jpg
Lang: es
Og_video: https://archive.org/download/libreweb/freakspot.webm
Slug: como-generar-freakspot
Status: published
Tags: educación, GNU/Linux, Pelican, Python, software libre, tutorial, video
Time: 16:58
Title: ¿Cómo generar Freak Spot?

<figure>
  <video id="player-ply" playsinline controls>
    <source src="https://archive.org/download/libreweb/freakspot.webm" type="video/webm">
  </video>
  <figcaption>
    <h1>¿Cómo generar Freak Spot?</h1>
  </figcaption>
</figure>

Alguno que otro usuario se ha preguntado alguna vez
cómo generar el sitio web [freakspot][site]{:target="_blank" rel="noopener noreferrer"},
la verdad es que es bastante sencillo una vez explicado.
Es por ello que en el siguiente vídeo se detallan los pasos a seguir.

<details markdown="span">
<summary>Mostrar Más</summary>
<div class="mt-2">
<ol>
<li>Clonación del repositorio e ingreso al directorio de trabajo
~~~~{.bash}
git clone https://notabug.org/Freak-Spot/Freak-Spot
cd Freak-Spot
~~~~
</li>

<li>Generar el entorno virtual y activarlo
~~~~{.bash}
virtualenv env
source env/bin/activate
~~~~
</li>

<li>Instalación de dependencias pip y npm
~~~~{.bash}
sudo npm install uglifycss uglify-js -g
pip install -U pelican beautifulsoup4 markdown babel
~~~~
</li>

<li>Generar el sitio web
~~~~{.bash}
$ (cd freak-theme && make compile)
$ make html
~~~~
</li>
</ol>
</div>

</details>

[site]: https://freakspot.net/
