Author: Free Software Foundation
Category: GNU
Date: 2008-09-02 21:04:48
Image: 2008/09/happy-gnu.jpg
License: CC BY-ND 3.0
Og_video: https://archive.org/download/libreweb/sf-large.webm
Slug: happy-birthday-to-gnu
Tags: GNU
Time: 5:51
Title: Happy Birthday To GNU

<figure>
<video id="player-ply" playsinline controls poster="{filename}/wp-content/uploads/article/images/2008/09/poster-happy-gnu.png">
  <source src="https://archive.org/download/libreweb/sf-large.webm" type="video/webm">
  <track kind="captions" label="Arabic" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-arabic.vtt" srclang="ar">
  <track kind="captions" label="Belarusian" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-belarus.vtt" srclang="be">
  <track kind="captions" label="Bulgarian" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-bulgarian.vtt" srclang="bg">
  <track kind="captions" label="Catalan" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-catalan.vtt" srclang="ca">
  <track kind="captions" label="Chinese" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-chinese.vtt" srclang="zh">
  <track kind="captions" label="Danish" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-danish.vtt" srclang="da">
  <track kind="captions" label="Dutch" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-dutch.vtt" srclang="nl">
  <track kind="captions" label="English" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-english.vtt" srclang="en">
  <track kind="captions" label="Español" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-spanish.vtt" srclang="es" default>
  <track kind="captions" label="Esperanto" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-esperanto.vtt" srclang="eo">
  <track kind="captions" label="Finnish" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-finnish.vtt" srclang="fi">
  <track kind="captions" label="French" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-french.vtt" srclang="fr">
  <track kind="captions" label="Galician" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-galego.vtt" srclang="gl">
  <track kind="captions" label="German" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-german.vtt" srclang="de">
  <track kind="captions" label="Greek" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-greek.vtt" srclang="el">
  <track kind="captions" label="Hebrew" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-hebrew.vtt" srclang="he">
  <track kind="captions" label="Hindi" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-hindi.vtt" srclang="hi">
  <track kind="captions" label="Hungarian" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-hungarian.vtt" srclang="hu">
  <track kind="captions" label="Icelandic" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-icelandic.vtt" srclang="is">
  <track kind="captions" label="Indonesian" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-indonesian.vtt" srclang="id">
  <track kind="captions" label="Italian" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-italian.vtt" srclang="it">
  <track kind="captions" label="Japanese" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-japanese.vtt" srclang="ja">
  <track kind="captions" label="Kannada" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-kannada.vtt" srclang="kn">
  <track kind="captions" label="Korean" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-korean.vtt" srclang="ko">
  <track kind="captions" label="Malayalam" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-malayalam.vtt" srclang="ml">
  <track kind="captions" label="Nepali" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-nepali.vtt" srclang="ne">
  <track kind="captions" label="Norwegian bokmål" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-nb.vtt" srclang="nb">
  <track kind="captions" label="Persian" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-persian.vtt" srclang="fa">
  <track kind="captions" label="Polish" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-polish-utf-8.vtt" srclang="pl">
  <track kind="captions" label="Portuguese" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-brazilian-portuguese.vtt" srclang="pt">
  <track kind="captions" label="Romanian" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu.ro.vtt" srclang="ro">
  <track kind="captions" label="Russian" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-russian.vtt" srclang="ru">
  <track kind="captions" label="Slovak" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-slovak.vtt" srclang="sk">
  <track kind="captions" label="Swedish" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-swedish.vtt" srclang="sv">
  <track kind="captions" label="Turkish" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu.tr.vtt" srclang="tr">
  <track kind="captions" label="Chinese (Simplified)" src="{filename}/wp-content/uploads/article/subtitles/2008/09/happy-birthday-to-gnu-zh_CN.vtt" srclang="zh-Hans">
</video>
<figcaption>
  <h1>Happy Birthday To GNU</h1>
</figcaption>
</figure>

Sr. Stephen Fry introduces you to free software, and reminds you of a very special birthday.
In this film, Stephen Fry, the actor, humorist and author introduces the viewer to free software and GNU.
Along the way, he compares proprietary software to the 'tyranny' of scientific research that cannot be shared,
studied and distributed by others.

<details markdown="span">
<summary>Show More</summary>
<ul>
<li>[Credits][credits]{:target="_blank" rel="noopener noreferrer"} of video</li>
<li>[Free distros][freedistros]{:target="_blank" rel="noopener noreferrer"}</li>
</ul>
</details>

[credits]: https://www.gnu.org/fry/happy-birthday-to-gnu-credits.html
[freedistros]: https://www.gnu.org/distros/free-distros.html
