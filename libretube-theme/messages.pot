# Translations template for PROJECT.
# Copyright (C) 2020 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2020-06-17 20:01-0500\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.7.0\n"

#: templates/archives.html:5
#, python-format
msgid "Archivos de %(sitename)s"
msgstr ""

#: templates/archives.html:9
#, python-format
msgid "Lista de artículos publicados en %(sitename)s"
msgstr ""

#: templates/archives.html:10
msgid "archivos,artículos,historia,lista de artículos"
msgstr ""

#: templates/archives.html:57
#, python-format
msgid "%(num)d artículo"
msgid_plural "%(num)d artículos"
msgstr[0] ""
msgstr[1] ""

#: templates/article.html:92
msgid "Licencia"
msgstr ""

#: templates/article.html:177
msgid "Publicado el"
msgstr ""

#: templates/article.html:180
#, python-format
msgid "Vídeos de %(nombre_mes)s de %(año)s"
msgstr ""

#: templates/article.html:183
msgid "de"
msgstr ""

#: templates/article.html:185
#, python-format
msgid "Vídeos de %(año)s"
msgstr ""

#: templates/article.html:199
#, python-format
msgid ""
"Lo siento, el sistema de comentarios no funciona sin JavaScript. Si "
"deseas, puedes %(abre_enlace)s enviar tu comentario por correo "
"electrónico %(cierra_enlace)s. El comentario será publicado en el espacio"
" reservado a comentarios de esta página."
msgstr ""

#: templates/article.html:248 templates/base.html:113
#, python-format
msgid "Ver %(articulo)s"
msgstr ""

#: templates/article_info.html:16 templates/article_info_aside.html:16
#, python-format
msgid "Artículos de %(nombre_mes)s de %(año)s"
msgstr ""

#: templates/article_info.html:20 templates/article_info_aside.html:20
#, python-format
msgid "Artículos de %(año)s"
msgstr ""

#: templates/article_info.html:32
msgid "Fecha de modificación"
msgstr ""

#: templates/article_info.html:47 templates/article_info_aside.html:34
msgid "Autor del artículo"
msgstr ""

#: templates/author.html:5 templates/author.html:11
#, python-format
msgid "Artículos escritos por %(author)s en %(sitename)s"
msgstr ""

#: templates/author.html:12
#, python-format
msgid "articulista,artículos,author,autor %(author)s,escritor,%(author)s"
msgstr ""

#: templates/author.html:28
#, python-format
msgid "Autor: %(author)s"
msgstr ""

#: templates/authors.html:5
#, python-format
msgid "Créditos de %(sitename)s"
msgstr ""

#: templates/authors.html:9 templates/authors.html:14 templates/authors.html:20
#, python-format
msgid "Lista de autores de %(sitename)s"
msgstr ""

#: templates/authors.html:10
msgid "articulistas, autores, colaboradores, escritores, lista de autores"
msgstr ""

#: templates/authors.html:31
msgid "Artículos de vídeo"
msgstr ""

#: templates/authors.html:36
#, python-format
msgid "Artículos escritos por %(author)s"
msgstr ""

#: templates/authors.html:42
msgid "Programación"
msgstr ""

#: templates/authors.html:44
msgid "Sitio para gente Libre"
msgstr ""

#: templates/authors.html:45
msgid "Sitio web de Jesús E."
msgstr ""

#: templates/authors.html:48
msgid "Traducción"
msgstr ""

#: templates/authors.html:50
msgid "Sitio web de Alyssa Rosenzweig"
msgstr ""

#: templates/authors.html:54
msgid "Software que usa la página"
msgstr ""

#: templates/authors.html:61
msgid "Algunos programas de JavaScript"
msgstr ""

#: templates/authors.html:64
#, python-format
msgid "Sitio web de %(programa)s"
msgstr ""

#: templates/base.html:38
msgid "Escribe para buscar..."
msgstr ""

#: templates/base.html:48
msgid "VideoTeca"
msgstr ""

#: templates/base.html:66
msgid "Inicio"
msgstr ""

#: templates/base.html:70
msgid "Archivos"
msgstr ""

#: templates/base.html:93
msgid "Ver ahora"
msgstr ""

#: templates/base.html:161 templates/base.html:163
msgid "Política de uso"
msgstr ""

#: templates/base.html:168
msgid "Licencias de JavaScript"
msgstr ""

#: templates/base.html:172
msgid "Código fuente"
msgstr ""

#: templates/base.html:179
msgid "Versión Actual:"
msgstr ""

#: templates/categories.html:5 templates/categories.html:8
#, python-format
msgid "Lista de categorías de %(sitename)s"
msgstr ""

#: templates/categories.html:9
msgid "categorías"
msgstr ""

#: templates/categories.html:21
msgid "Lista de categorías"
msgstr ""

#: templates/index.html:5
msgid "Página"
msgstr ""

#: templates/index.html:15
msgid "Videoteca de software libre brindando avance tecnológico"
msgstr ""

#: templates/period_archives.html:7 templates/period_archives.html:27
#, python-format
msgid "Archivos de %(año)d"
msgstr ""

#: templates/period_archives.html:9 templates/period_archives.html:29
#, python-format
msgid "Archivos de %(mes)s de %(año)d"
msgstr ""

#: templates/period_archives.html:11 templates/period_archives.html:31
#, python-format
msgid "Archivos del %(dia)d de %(mes)s de %(año)d"
msgstr ""

#: templates/tags.html:5 templates/tags.html:8
#, python-format
msgid "Lista de etiquetas de %(sitename)s"
msgstr ""

#: templates/tags.html:9
msgid "etiquetas,palabras clave"
msgstr ""

#: templates/tags.html:16
msgid "Lista de etiquetas"
msgstr ""

#: templates/translations.html:3
msgid "Traducciones:"
msgstr ""

