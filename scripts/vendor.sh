#!/bin/bash

run_vendor()
{

    local LOCALDIR="${2-$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)}"
    local TARGET="${LOCALDIR%/*}" # deleted slash
    local BASE="${TARGET%scripts}" # input

    if [[ -d "$BASE"/local.env/hashover-next/ ]];
    then
        rm -rf "$BASE"/output/hashover-next/
        echo "$BASE"/output/ | xargs -n 1 cp -rf "$BASE"/local.env/hashover-next/
    else
        echo 'Not hashover'
    fi

    if [[ -f "$BASE"/local.env/.htaccess ]];
    then
        rm -rf "$BASE"/output/.htaccess
        cp -rf "$BASE"/local.env/.htaccess "$BASE"/output/
    else
        echo 'Not exits .htaccess'
    fi

}

# ==============================================================================
# EXECUTION - START
# ==============================================================================
run_vendor "$@"
